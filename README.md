# downstreamjobs1

This example of downstream jobs was made directly on Jenkins UI, using the Pipeline Script of the job configuration. 
What this achieves is to represent how a job can call another using the following: 
            script{
                build job: 'downstream2',
                parameters: [
                    [ $class: 'StringParameterValue', name: 'FROM_BUILD', value: "${BUILD_NUMBER}"]
                    ]
            }

downstream2 job receives the parameters used on the downstream1 job ad prints the result. A more complex example will be built. 

